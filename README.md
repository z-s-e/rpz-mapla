# rpz-mapla prototype

A prototype system for a network audio stream player based on [Advanced Linux Playback Engine](https://gitlab.com/z-s-e/alpe).

For this you need:

- A Raspberry Pi Zero W
- A [HiFiBerry DAC+ Zero](https://www.hifiberry.com/shop/boards/hifiberry-dac-zero/)

Brief instructions:

- `make BR2_EXTERNAL=/path/to/repo/buildroot-2023.11.1 rpz_mapla_defconfig`
- `make menuconfig` -> External options -> enter Wifi settings and save
    - Warning: the Wifi PSK will be stored on the image unencrypted and it runs a password-less ssh server - only use in safe environment
- `make` and copy resulting sdcard.img to an sd card
