#!/bin/sh

set -e
set -u

COUNTRY=$1
SSID=$2
PSK=$3

echo "country=${COUNTRY}" >> wpa.conf
echo "network={" >> wpa.conf
echo "  ssid=\"${SSID}\"" >> wpa.conf
echo "  psk=\"${PSK}\"" >> wpa.conf
echo "}" >> wpa.conf
