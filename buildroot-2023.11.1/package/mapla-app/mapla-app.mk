MAPLA_APP_VERSION = 8dc8b78829c478129c740d8eb5f1502ae90cabce
MAPLA_APP_SITE = https://gitlab.com/z-s-e/mapla.git
MAPLA_APP_SITE_METHOD = git
MAPLA_APP_GIT_SUBMODULES = YES
MAPLA_APP_DEPENDENCIES = host-pkgconf alsa-lib

$(eval $(cmake-package))
