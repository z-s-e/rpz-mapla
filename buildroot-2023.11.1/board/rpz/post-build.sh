#!/bin/sh

set -e

SCRIPTDIR=$(dirname $(readlink -f "$0"))

RNGD_INIT_ORIG="${TARGET_DIR}/etc/init.d/S21rngd"
RNGD_INIT_WANT="${TARGET_DIR}/etc/init.d/S49rngd"

if [ -e ${RNGD_INIT_ORIG} ]; then
    if [ -e ${RNGD_INIT_WANT} ]; then
        rm ${RNGD_INIT_WANT}
    fi
    mv ${RNGD_INIT_ORIG} ${RNGD_INIT_WANT}
fi


FSTAB_ORIG="${TARGET_DIR}/etc/fstab"
FSTAB_WANT="${SCRIPTDIR}/fstab"

if [ -e ${FSTAB_ORIG} ]; then
    rm ${FSTAB_ORIG}
fi
cp ${FSTAB_WANT} ${FSTAB_ORIG}

if [ ! -d ${TARGET_DIR}/boot ]; then
    mkdir ${TARGET_DIR}/boot
fi
